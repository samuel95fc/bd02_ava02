package business;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

import domain.Editora;
import domain.Emprestimo;
import domain.Leitor;
import domain.Livro;
import domain.TipoLivroEnum;
import persistence.EmprestimoDTO;

public class Exemplo {

	public static void main(String[] args) {

		EntityManagerFactory emf = Persistence.createEntityManagerFactory("biblioteca");

		EntityManager em = emf.createEntityManager();
		
		em.getTransaction().begin();
		Editora editoraSaraiva = new Editora();
		editoraSaraiva.setNome("Saraiva");
		editoraSaraiva.setLivros(new ArrayList<Livro>());
		em.persist(editoraSaraiva);
		
		Livro livroBD = new Livro();
		livroBD.setIsbn("ABC123");
		livroBD.setNome("Banco de Dados");
		livroBD.setTipoLivro(TipoLivroEnum.CONSULTA);
		
		Livro livroCOMP = new Livro();
		livroCOMP.setIsbn("CBA321");
		livroCOMP.setNome("Compiladores");
		livroCOMP.setTipoLivro(TipoLivroEnum.EMPRESTIMO);
		
		Livro livroED = new Livro();
		livroED.setIsbn("DFE456");
		livroED.setNome("Estrutura de Dados");
		livroED.setTipoLivro(TipoLivroEnum.EMPRESTIMO);
		
		Leitor leitorSamuel = new Leitor();
		leitorSamuel.setCpf("12345678909");
		leitorSamuel.setNome("Samuel Jesus");
		leitorSamuel.setTelefone(new ArrayList<String>());
		
		LocalDate dateCOMP = LocalDate.of(2020, 8, 11);
		Emprestimo emprestimoCOMP = new Emprestimo();
		emprestimoCOMP.setLeitor(leitorSamuel);
		emprestimoCOMP.setLivro(livroCOMP);
		emprestimoCOMP.setDataEmprestimo(dateCOMP);
		
		LocalDate dateED = LocalDate.of(2020, 7, 10);
		Emprestimo emprestimoED = new Emprestimo();
		emprestimoED.setLeitor(leitorSamuel);
		emprestimoED.setLivro(livroED);
		emprestimoED.setDataEmprestimo(dateED);
		
		em.getTransaction().commit();		
		
		String oql = "select new " + EmprestimoDTO.class.getCanonicalName()
				+ "(l.*, count(*) from emprestimo e inner join livro l group by l.tipoLivro";

		TypedQuery<EmprestimoDTO> queryEmprestimo = em.createQuery(oql, EmprestimoDTO.class);
		List<EmprestimoDTO> result = queryEmprestimo.getResultList();
		System.out.println("consultarQtdEmprestimoPorLivro");
		for (EmprestimoDTO emprestimoDTO : result) {
			System.out.println(emprestimoDTO);
		}

		
		TypedQuery<Livro> queryLivro = em.createNamedQuery("livros por tipo", Livro.class);
		queryLivro.setParameter("tipoLivro", TipoLivroEnum.EMPRESTIMO);
		List<Livro> livros = queryLivro.getResultList();
		System.out.println("consultarPorTipo:");
		for (Livro livro : livros) {
			System.out.println(livro);
		}

	}

}
