package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQuery;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import hibernate.PostgreSQLEnumType;

@Entity(name = "tab_livro")
@TypeDef(name = "pgsql_enum", typeClass = PostgreSQLEnumType.class)
@NamedQuery(query = "select * from livro l where l.tipoLivro = :tipoLivro", 
name = "livros por tipo")
public class Livro {

	@Id
	@Column(name = "liv_isbn", length = 200, nullable = false)
	private String isbn;

	@Column(name = "liv_nome", length = 200, nullable = false)
	private String nome;

	@Enumerated(EnumType.STRING)
	@Column(columnDefinition = "tipo_livro")
	@Type(type = "pgsql_enum")
	private TipoLivroEnum tipoLivro;

	public Livro() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Livro(String isbn, String nome, TipoLivroEnum tipoLivro) {
		super();
		this.isbn = isbn;
		this.nome = nome;
		this.tipoLivro = tipoLivro;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public TipoLivroEnum getTipoLivro() {
		return tipoLivro;
	}

	public void setTipoLivro(TipoLivroEnum tipoLivro) {
		this.tipoLivro = tipoLivro;
	}

	@Override
	public String toString() {
		return "Livro [isbn=" + isbn + ", nome=" + nome + ", tipoLivro=" + tipoLivro + "]";
	}
}
