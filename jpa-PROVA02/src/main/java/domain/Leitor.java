package domain;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "tab_leitor", schema = "public", uniqueConstraints = {
		@UniqueConstraint(name = "un_cpf", columnNames = { "num_cpf" }) }, indexes = {
				@Index(name = "ndx_leitor", unique = false, columnList = "nome") })
public class Leitor {
	
	@Column(name = "num_cpf")
	private String cpf;
	
	@Column(length = 50, nullable = false)
	private String nome;
	
	@Embedded
	@ElementCollection
	private List<String> telefone;
	
	public Leitor() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Leitor(String cpf, String nome, List<String> telefone) {
		super();
		this.cpf = cpf;
		this.nome = nome;
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<String> getTelefone() {
		return telefone;
	}

	public void setTelefone(List<String> telefone) {
		this.telefone = telefone;
	}

	@Override
	public String toString() {
		return "Leitor [cpf=" + cpf + ", nome=" + nome + ", telefone=" + telefone + "]";
	}
	
	
}
