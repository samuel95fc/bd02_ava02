package domain;

public enum TipoLivroEnum {
	
	CONSULTA ("C"), EMPRESTIMO ("E");

	private String codigo;

	private TipoLivroEnum(String codigo) {

		this.codigo = codigo;
	}

	public String getCodigo() {
		return codigo;
	}

	public static TipoLivroEnum valueOfCodigo(String codigo) {
		for (TipoLivroEnum tipoLivroEnum : values()) {
			if (tipoLivroEnum.getCodigo().equals(codigo)) {
				return tipoLivroEnum;
			}
		}
		throw new IllegalArgumentException();
	}
}
