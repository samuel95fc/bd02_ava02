package domain;

import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

@Entity(name = "tab_emprestimo")
@SequenceGenerator(name="seq_emprestimo", sequenceName="sq_emprestimo", schema="public")
public class Emprestimo {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_emprestimo")
	private Long id;
	
	@OneToOne(cascade=CascadeType.ALL, mappedBy="livro", fetch=FetchType.EAGER, optional=false, orphanRemoval=true)
	private Livro livro;
	
	@OneToOne(cascade=CascadeType.ALL, mappedBy="leitor", fetch=FetchType.EAGER, optional=false, orphanRemoval=true)
	private Leitor leitor;
	
	@Column(name = "emp_dt_emprestimo")
	private LocalDate dataEmprestimo;
	
	@Column(name = "emp_dt_devolucao")
	private LocalDate dataDevolucao;
	
	public Emprestimo() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Emprestimo(Long id, Livro livro, Leitor leitor, LocalDate dataEmprestimo, LocalDate dataDevolucao) {
		super();
		this.id = id;
		this.livro = livro;
		this.leitor = leitor;
		this.dataEmprestimo = dataEmprestimo;
		this.dataDevolucao = dataDevolucao;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Livro getLivro() {
		return livro;
	}

	public void setLivro(Livro livro) {
		this.livro = livro;
	}

	public Leitor getLeitor() {
		return leitor;
	}

	public void setLeitor(Leitor leitor) {
		this.leitor = leitor;
	}

	public LocalDate getDataEmprestimo() {
		return dataEmprestimo;
	}

	public void setDataEmprestimo(LocalDate dataEmprestimo) {
		this.dataEmprestimo = dataEmprestimo;
	}

	public LocalDate getDataDevolucao() {
		return dataDevolucao;
	}

	public void setDataDevolucao(LocalDate dataDevolucao) {
		this.dataDevolucao = dataDevolucao;
	}

	@Override
	public String toString() {
		return "Emprestimo [id=" + id + ", livro=" + livro + ", leitor=" + leitor + ", dataEmprestimo=" + dataEmprestimo
				+ ", dataDevolucao=" + dataDevolucao + "]";
	}
	
	
}
