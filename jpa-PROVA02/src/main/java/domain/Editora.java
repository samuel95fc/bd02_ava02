package domain;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

@Entity(name = "tab_editora")
@SequenceGenerator(name="seq_editora", sequenceName="sq_editora", schema="public")
public class Editora {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator="seq_editora")
	private Integer id;
	
	@Column(length = 200, nullable = false)
	private String nome;
	
	@OneToMany(cascade=CascadeType.ALL, fetch=FetchType.LAZY, mappedBy="", orphanRemoval=true, targetEntity=Livro.class)
	private List<Livro> livros;
	
	public Editora() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Editora(Integer id, String nome, List<Livro> livros) {
		super();
		this.id = id;
		this.nome = nome;
		this.livros = livros;
	}
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Livro> getLivros() {
		return livros;
	}

	public void setLivros(List<Livro> livros) {
		this.livros = livros;
	}

	@Override
	public String toString() {
		return "Editora [id=" + id + ", nome=" + nome + ", livros=" + livros + "]";
	}
	
}
