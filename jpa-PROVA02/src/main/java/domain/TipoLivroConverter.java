package domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class TipoLivroConverter implements AttributeConverter<TipoLivroEnum, String> {
	
	@Override
	public String convertToDatabaseColumn(TipoLivroEnum attribute) {
		return attribute == null ? null : attribute.getCodigo();
	}

	@Override
	public TipoLivroEnum convertToEntityAttribute(String dbData) {
		return dbData == null ? null : TipoLivroEnum.valueOfCodigo(dbData);
	}

}
