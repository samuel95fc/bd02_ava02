package persistence;

import domain.TipoLivroEnum;

public class EmprestimoDTO {
	
	private TipoLivroEnum tipoLivro;
	
	private Long quantidade;
	
	public EmprestimoDTO(TipoLivroEnum tipoLivro, Long quantidade) {
		super();
		this.tipoLivro = tipoLivro;
		this.quantidade = quantidade;
	}

	public TipoLivroEnum getTipoLivro() {
		return tipoLivro;
	}

	public void setTipoLivro(TipoLivroEnum tipoLivro) {
		this.tipoLivro = tipoLivro;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public String toString() {
		return "EmprestimoDTO [tipoLivro=" + tipoLivro + ", quantidade=" + quantidade + "]";
	}
}
